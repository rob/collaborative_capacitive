

# CAPACITIVE COLLABORATIVE 
- INSTRUMENT BUILDING WORKSHOP
- PIFCAMP 2023

## DEPENDENCIES

- pyserial - sudo apt install python3-serial

- ARDUINO 2.1.1

- esp boards for arduino

grep doit ./Arduino/hardware/espressif/esp32/boards.txt


mpr121 library by Adafruit
neopixel library by Adafruit

modify boards.txt to allow large partition with no OTA
=
esp32doit-devkit-v1.menu.PartitionScheme.no_ota=No OTA (Large APP)
esp32doit-devkit-v1.menu.PartitionScheme.no_ota.build.partitions=no_ota
esp32doit-devkit-v1.menu.PartitionScheme.no_ota.upload.maximum_size=2097152

- OSC by Adrian Freed

oscdump for testing - sudo apt install liblo-utils

Listen to OSC on PORT 57120
-----------------
# GPIO pin  = touch input mapping
byte keys[] = {
32, // 0
33, // 1
27, // 2
14, // 3
12, // 4
13, // 5
-----------
4, // 6
15 // 7
};


