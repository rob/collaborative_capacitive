// esp32 capacitive touch interface /////////////////////////////////////////////////
// rob canning 2023 /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
// flash mode changed to DIO !!!
// NEOPIXEL ////////////////////////////////////////////////////
#include <WS2812FX.h> // https://github.com/kitesurfer1404/WS2812FX

// On the ESP32S2 SAOLA GPIO is the NeoPixel.
#define PIN        25 
//Single NeoPixel

WS2812FX ws2812fx = WS2812FX(12, PIN, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 25 // Time (in milliseconds) to pause between color change
//#include <SPI.h> // Serial Peripheral Interface

// MPR121 /////////////////////////////////////////////////////
#include <Wire.h> // allows you to communicate with I2C / TWI devices
#include <Adafruit_MPR121.h>

#ifndef _BV
#define _BV(bit) (1 << (bit)) 
#endif
// You can have up to 4 on one i2c bus but one is enough for testing!
Adafruit_MPR121 cap0 = Adafruit_MPR121();
Adafruit_MPR121 cap1 = Adafruit_MPR121();

// Keeps track of the last pins touched
// so we know when buttons are 'released'
uint16_t lasttouched[2];
uint16_t currtouched[2];

// Wi-Fi SETTINGS /////////////

#include <WiFi.h>
const char* ssid     = "zavod rizoma";
const char* password = "cermozise";
// BROADCAST TO ALL IP / port
const IPAddress castIp = IPAddress(192,168,0,255);  // 255 FOR BROADCAST
const int port = 57120; // SUPERCOLLIDERS DEFAULT INCOMING OSC PORT
const unsigned int localPort = 8888;        // local port to listen for UDP packets (here's where we send the packets)

bool connected = false;
#include <WiFiUdp.h>
WiFiUDP udp;

// OSC and serial communications  ////////////////////////////

#define SERIAL_OSC
#define WIFI_OSC
#include <OSCBundle.h>
#include <OSCMessage.h>
#include <OSCBoards.h>
#include <OSCData.h>

/* OSC MSG channels */
OSCBundle bundle;

OSCErrorCode error;
unsigned int ledState = LOW;              // LOW means led is *on*
unsigned int ledNum = 0;              // LOW means led is *on*
unsigned int touchBoard = 0;              // LOW means led is *on*

#ifndef BUILTIN_LED
#ifdef LED_BUILTIN
#define BUILTIN_LED LED_BUILTIN
#else
#define BUILTIN_LED 13
#endif
#endif


// SERIAL /////////////////////////////////
#ifdef BOARD_HAS_USB_SERIAL
#include <SLIPEncodedUSBSerial.h>
SLIPEncodedUSBSerial SLIPSerial( thisBoardsSerialUSB );
#else
#include <SLIPEncodedSerial.h>
SLIPEncodedSerial SLIPSerial(Serial);
#endif


///////////////////////////////////////////

byte KEYLEN = 12;

void setup() {
  ws2812fx.init();
  ws2812fx.setBrightness(10);
     //ws2812fx.setPixelColor(11, ws2812fx.Color(0,0,0));
     //

  // parameters: index, start, stop, mode, color, speed, reverse
 // ws2812fx.setSegment(0,  0,  11, FX_MODE_BREATH, 0xFFFFFF, 1000, false); // segment 0 is leds 0 - 9
  //ws2812fx.setSegment(1, 0, 0, FX_MODE_BREATH,  0xFF0000, 300, false); // segment 1 is leds 10 - 19
  //ws2812fx.setSegment(2, 6, 11, FX_MODE_BREATH, 0x0000FF, 400, true);  // segment 2 is leds 20 - 29
  
  ws2812fx.setSegment(0, 0, 11, 11, BLACK, 2000, false);    

  ws2812fx.start();

  Serial.begin(115200);
  WiFi.mode(WIFI_AP);
  connectWiFi();
  
  udp.begin(localPort); // start udp for incoming OSC messages

  const String macAddress = WiFi.macAddress(); ;
  
  // Default address is 0x5A, if tied to 3.3V its 0x5B         
  // If tied to SDA its 0x5C and if SCL then 0x5D
  if (!cap0.begin(0x5A)) {                                          
    Serial.println("MPR121 A not found, check wiring?");             
    while (1);
  } Serial.println("MPR121 A found!");  
  
  
  if (!cap1.begin(0x5C)) {                                          
      Serial.println("MPR121 B not found, check wiring?");             
    while (1);
    } Serial.println("MPR121 B found!");
      

//cap0.setThreshholds(20, 6);
//cap1.setThreshholds(20, 6);

#ifdef SERIAL_OSC
  SLIPSerial.begin(115200);   // set this as high as you can reliably run on your platform 
#endif
  
} // setup ends //////////////////////////////////////////////////


// MAIN LOOP /////////////////////////////////////////////////////////

int colorCount = 0;

void loop() { 
  rOSC();
  //ws2812fx.service();   
  sendCapacitive(cap0, 0); // from the 121
  sendCapacitive(cap1, 1); // from the 121  
}

// FUNCTIONS /////////////////////////////////////////////

void connectWiFi(){
  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    ws2812fx.setPixelColor(0, ws2812fx.Color(0,3,222));
    delay(500);
    Serial.print(".");
  }
  
  // Print local IP address and start web server
  //  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

//NEOPIXEL COLOUR LOGIC /////////////////////////////////////////////

uint32_t colors[] = {RED, GREEN, BLUE, YELLOW, CYAN, MAGENTA, PURPLE, ORANGE, PINK, GRAY, ULTRAWHITE, WHITE, BLACK };

void led(OSCMessage &msg) {
  
  ledNum = msg.getInt(0); // which sensor/led 
  ledState = msg.getInt(1); // which state
  touchBoard = msg.getInt(2); // which touch board

  // increment led index based on which board input i.e. board 1 is 0-11, board 2 is 12 - 24
  if (touchBoard==0){ledNum = ledNum + 0; };
  if (touchBoard==1){ledNum = ledNum+12; }; // change to 12 when new strip wired! 6 for testing

  switch (msg.getInt(1)) {
          ws2812fx.setMode(FX_MODE_BLINK);  
    case 0:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
    case 1:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    case 5:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
      case 6:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
  }

  switch (msg.getInt(1)) {
          ws2812fx.setMode(FX_MODE_BLINK);  
    case 0:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
    case 1:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
    case 2:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
    case 3:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
    case 4:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
      case 5:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
      case 6:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
      case 7:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
          case 8:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
          case 9:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
          case 10:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;

          case 11:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
              case 12:
      ws2812fx.setPixelColor(ledNum, colors[ledState]);
    break;
ws2812fx.start();

}

    //ws2812fx.start();
    ws2812fx.show();

}

void flashyNeoPixel(int x){
Serial.print('asfd : ' + x);
 if (x==2){
   while(x==2){
     ws2812fx.setPixelColor(11, ws2812fx.Color(0,222,222));
     ws2812fx.show();
     delay(100);
     ws2812fx.setPixelColor(11, ws2812fx.Color(222,222,222));
     ws2812fx.show();
     delay(100);
     }
 }
}

void rOSC(){
      //Serial.print("osc func called...");
 OSCMessage msg;
  int size = udp.parsePacket();

  if (size > 0) {
    while (size--) {
      msg.fill(udp.read());
    }
    if (!msg.hasError()) {
      msg.dispatch("/led", led);
    } else {
      error = msg.getError();
      Serial.print("error: ");
      Serial.println(error);
    }
  }
}

void sendCapacitive(Adafruit_MPR121 sensorboard, int index){ 
 // Get the currently touched pads
  currtouched[index] = sensorboard.touched() ;
    
  for (uint8_t i=0; i<KEYLEN; i++) {
   // it if *is* touched and *wasnt* touched before, alert!
    if ((currtouched[index] & _BV(i)) && !(lasttouched[index] & _BV(i)) ) {
      Serial.print(i); Serial.println(" touched");
//      ws2812fx.setSegment(0,  i,  i, FX_MODE_STATIC, 0x0096FF, 700, false); // segment 0 is leds 0 - 9

      //ledState = !ledState;
              OSCMessage msg("/touch");
              msg.add(WiFi.macAddress().c_str()); // mac address of sender esp32           
              msg.add((int32_t)i); // send which sensor (0 - 11) was touched
              msg.add((int32_t)1); // 1 is for touched - below 0 is for release
              msg.add((int32_t)index); // which sensor board was touched (0 - 1)

              #ifdef WIFI_OSC
              udp.beginPacket(castIp, port);
            //bundle.send(udp);
              msg.send(udp);
              udp.endPacket();
             #endif

            String cap = "val for key num: " + String(i) + " === " + String(HIGH);
            //Serial.println("OSC SENT "); Serial.println(cap);       
    }
    // if it *was* touched and now *isnt*, alert!
    if (!(currtouched[index] & _BV(i)) && (lasttouched[index] & _BV(i)) ) {
      Serial.print(i); Serial.println(" released");
      //ws2812fx.setSegment(0,  i,  i, FX_MODE_STATIC, BLACK, 800, false); // segment 0 is leds 0 - 9      
      //ws2812fx.setSegment(1,  0,  11, FX_MODE_BREATH, 0xFF0000, 500, false); // segment 0 is leds 0 - 9      

      OSCMessage msg("/touch");
      msg.add(WiFi.macAddress().c_str()); // mac address of sender           
      msg.add((int32_t)i); // send which sensor was touched
      msg.add((int32_t)0); // 
      msg.add((int32_t)index); // index of sensor board
      
#ifdef WIFI_OSC
      udp.beginPacket(castIp, port);
      //bundle.send(udp);
      msg.send(udp);
      udp.endPacket();
#endif
      
     // String cap = "val for key num: " + String(i) + " === " + String(LOW);
     // Serial.println("OSC SENT "); Serial.println(cap);       
    }
  }

  // reset our state
  lasttouched[index] = currtouched[index];
  // put a delay so it isn't overwhelming
  delay(5);

} //////////////////
