# autostart file for miza installation

killall -9 /usr/lib/ardour7/ardour-7.3.0~ds0

killall -9 liquidsoap qjackctl jackd sclang scide;

barrier &

# choose audio interface
/usr/bin/jackd  -aa -u -dalsa -r48000 -p256 -n3 -D -Chw:U192k -Phw:U192k &
#usr/bin/jackd  -aa -u -dalsa -r48000 -p256 -n3 -D -Chw:VSL -Phw:VSL &

sleep 4;

qjackctl  -a ~/spellcaster/spellcaster.xml &

sleep 2;

scide ~/pifcamp2023/sc/collaborative_capacitive.scd &

#sleep 1;

#ardour ~/spellcaster/ardour/spellcaster/spellcaster.ardour &

#sleep 1;

#liquidsoap ~/spellcaster/spellcaster.liq &
#xterm -e "liquidsoap  ~/spellcaster/spellcaster.liq "

#sleep 1;

xterm; &
