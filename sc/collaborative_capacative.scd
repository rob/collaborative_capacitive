/*RUN*/

s.options.numInputBusChannels = 4;
s.options.numOutputBusChannels = 4;

//OSCFunc.trace(true); // Turn posting on
//OSCFunc.trace(false); // Turn posting off

s.waitForBoot{
	///////////////////////////////////////////////////////////////////////////
	// set path for sample location and make dictionary of subdirectories
	//~samplepath = thisProcess.nowExecutingPath.dirname  ++ "/samples";
	~samplepath = "/home/rob/pifcamp2023/sc/samples"; // CHANGE THIS TO YOUR OWN PATH
	~smp = Dictionary.new;

	PathName(~samplepath).entries.do{
		arg subfolder;
		~smp.add(
			subfolder.folderName.asSymbol ->
			Array.fill(
				subfolder.entries.size,
				{ arg i; Buffer.read(s, subfolder.entries[i].fullPath).normalize;}
			)
		);
	};
	// e.g. green sample subdirectory of ~samplepath:	~smp[\green][1] //
	///////////////////////////////////////////////////////////////////////

	// allocate busses
	~reverbBus = Bus.audio(s, 1);
	~ringModBus = Bus.audio(s, 1);

	// create group order
	~sGA = Group.new; ~sGB = Group.new; ~sGC = Group.new; ~sGD = Group.new;

	s.sync;

	//// SYNTHS //////////////////////////////////

	SynthDef.new(\splay, {| out=0, bufnum=0, t_trig=0, amp=0.5, rate=1,
		atk=1, rel=1, loop=0, spos=0, gate=0, panRate=0.2 |
		var env, sig;
		env= EnvGen.kr(Env.asr(atk,1,rel),gate, doneAction:2);
		sig = PlayBuf.ar(1, bufnum, rate, t_trig, startPos: spos, loop:loop, doneAction: 0);
		sig = sig * env * amp;
		sig = Pan2.ar(sig, FSinOsc.kr(panRate));
		Out.ar(out, sig);
	}).add;

	s.sync;

	~oPA = 8.collect({
		arg itt, rate=1, out=0, sdir=\bowl,
		amp=0.5, loop=1,  panRate=rrand(0.05,0.75), group=~sGA;
		//var deg = Scale.major[deg].midiratio;
		//postln( itt + "  bufnum" + \bufnum  + "sdir:" + sdir.asString + "amp: " + amp + "out: " + out);
		Synth.new(\splay,  [\bufnum, ~smp[sdir][rrand(0,~smp[sdir].size-1)], \out, out, \loop, loop,  panRate: panRate], group);
	});

	~oPB = 8.collect({
		arg itt, rate=1, out=0, sdir=\green,
		amp=0.1, loop=1,  panRate=rrand(0.05,0.75), group=~sGA;
		//var deg = Scale.major[deg].midiratio;
		//postln( itt + "  bufnum" + \bufnum  + "sdir:" + sdir.asString + "amp: " + amp + "out: " + out);
		Synth.new(\splay,  [\bufnum, ~smp[sdir][rrand(0,~smp[sdir].size-1)], \out, out, \loop, loop,  panRate: panRate], group);
	});


	// RECEIVE OSC MESSAGES FROM ESP32s

	~shift = 0; // shift state
	~ls0=1; // loop state
	s.sync;

	~esp32Receive = { arg name ,mac; OSCdef(name, { arg msg, time;

		// msg[1] sender mac address
		// msg[2] which of the [0 - 11] sensors is touched
		// msg[3] is it a touch [1] or a release [0]
		// msg[4] which of the connected mpr121 boards is sending

		if (msg[1] == mac && msg[3] == 1 && msg[4] == 0 && ~shift == 0 )  {
			postln("first board" + msg[2]);
			~scale = Scale.partch_o1;
			w = case
			{ msg[2] == 0} { ~oPA[0].set(\rate, ~scale[rrand(0,47)].midiratio, \t_trig, 1, \loop, 1, \gate, 1); }
			{ msg[2] == 1} { ~oPA[1].set(\rate, ~scale[rrand(0,47)].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 2} { ~oPA[2].set(\rate, ~scale[rrand(0,47)].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 3} { ~oPA[3].set(\rate, ~scale[rrand(0,47)].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 4} { ~oPA[4].set(\rate, ~scale[rrand(0,47)].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 5} { ~oPA[5].set(\rate, ~scale[rrand(0,47)].midiratio, \t_trig, 1, \gate, 1);  }
			{ msg[2] == 6} { ~oPA[6].set(\rate, ~scale[rrand(0,47)].midiratio, \t_trig, 1, \gate, 1);  }
			{ msg[2] == 7} { ~oPA[7].set(\rate, ~scale[rrand(0,47)].midiratio + 1 , \t_trig, 1, \gate, 1); }

			{ msg[2] == 8 } {  // touch 7 = toggle looping of all samplers
				~ls0 = (~ls0 + 1)% 2;
  				~sGA.set(\loop, ~ls0 ); // toggle this
				postln("loop state set as:" + ~ls0 + "    7 " + msg[2]  );
			};
		};

		if (msg[1] == mac && msg[3] == 1 && msg[4] == 1)  {
			postln("second board" + msg[2]);
			w = case
			{ msg[2] == 0} { ~oPB[0].set(\rate, Scale.major[0].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 1} { ~oPB[1].set(\rate, Scale.major[1].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 2} { ~oPB[2].set(\rate, Scale.major[2].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 3} { ~oPB[3].set(\rate, Scale.major[3].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 4} { ~oPB[4].set(\rate, Scale.major[4].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 5} { ~oPB[5].set(\rate, Scale.major[5].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 6} { ~oPB[6].set(\rate, Scale.major[6].midiratio, \t_trig, 1, \gate, 1); }
			{ msg[2] == 7} { ~oPB[7].set(\rate, Scale.major[7].midiratio, \t_trig, 1, \gate, 1); }

			{ msg[2] == 8 } {  // touch 7 = toggle looping of all samplers
				~ls0 = (~ls0 + 1)% 2;
				~sGB.set(\loop, ~ls0 ); // toggle this
				postln("loop state set as:" + ~ls0 + "    7 " + msg[2]  );
			};
		};

	// select with SHIFT key - toggle effect = apply with enter key
		// set a state // release of shift key acts as enter

		// msg[1] sender mac address
		// msg[2] which of the [0 - 11] sensors is touched
		// msg[3] is it a touch [1] or a release [0]
		// msg[4] which of the connected mpr121 boards is sending

		// if SHIFT IS DEPRESSED on board 1 - shift enabled while 11 depressed
		if (msg[1] == mac && msg[2] == 11 && msg[4] == 0 )  {
			if (msg[3]==1){
				~shift = 1; postln("shift is enabled"  )}
			{   ~shift = 0; postln("shift is disabled" )}
		};

		// when finished apply effect/change and set state to unselected

		if (~shift==1)  {
			postln(" shift is pressed " + msg[2]);
			w = case
			{ msg[2] == 0} { ~oPA[0].set(\rate, 16); }
			{ msg[2] == 1} { ~oPA[1].set(\rate, 14); }
			{ msg[2] == 2} { ~oPA[2].set(\rate, 12); }
			{ msg[2] == 3} { ~oPA[3].set(\rate, 10); }
			{ msg[2] == 4} { ~oPA[4].set(\rate, 8); }
			{ msg[2] == 5} { ~oPA[5].set(\rate, 6); }
			{ msg[2] == 6} { ~oPA[6].set(\rate, 4); }
			{ msg[2] == 7} { ~oPA[7].set(\rate, 2); }
		}
		//~oPA[0].get(\rate.poll).poll

	},'/touch') } ;

	~oPA[0].get(\rate).value;

	s.sync;

	~esp32Receive.('anything', '3C:E9:0E:AD:A5:48');

	~sG.set(\amp, 1);
	~sG.set(\rate, -1);

	~sGB.set(\amp, 0.2);


	"patch loaded and ready...".postln;

};
