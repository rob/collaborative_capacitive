// esp32 capacitive touch interface /////////////////////////////////////////////////
// rob canning 2023 /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

const int ledPin = 23;
const int shiftLedPin = 25;
const int loopLedPin = 26;

const int threshold = 14000;  // set the threshold
int ledState = LOW;         // the current state of the output pin
int shiftLedState = HIGH;         // the current state of the output pin
int loopLedState = LOW;         // the current state of the output pin

// NEOPIXEL ////////////////////////////////////////////////////

#include <Adafruit_NeoPixel.h>
// On the ESP32S2 SAOLA GPIO is the NeoPixel.
#define PIN        18 
//Single NeoPixel
Adafruit_NeoPixel pixels(1, PIN, NEO_GRB + NEO_KHZ800);
#define DELAYVAL 25 // Time (in milliseconds) to pause between color change
//#include <SPI.h> // Serial Peripheral Interface

// MPR121 /////////////////////////////////////////////////////
#include <Wire.h> // allows you to communicate with I2C / TWI devices
#include <Adafruit_MPR121.h>

#ifndef _BV
#define _BV(bit) (1 << (bit)) 
#endif/
.//./ You can have up to 4 on one i2c bus but one is enough for testing!

Adafruit_MPR121 cap0 = Adafruit_MPR121();

Adafruit_MPR121 cap1 = Adafruit_MPR121();
 pixels.begin(); // INITIALIZE NeoPixel (REQUIRED)



  pinMode(ledPin, OUTPUT); //Define ledPin as output
  pinMode(shiftLedPin, OUTPUT); //Define ledPin as output  
  pinMode(loopLedPin, OUTPUT); //Define ledPin as output  
  digitalWrite(ledPin, ledState);
  digitalWrite(shiftLedPin, shiftLedState);
  digitalWrite(loopLedPin, loopLedState);
  Serial.begin(115200);
  WiFi.mode(WIFI_AP);
  connectWiFi();
  
  const String macAddress = WiFi.macAddress(); ;

 // Default address is 0x5A, if tied to 3.3V its 0x5B         
  // If tied to SDA its 0x5C and if SCL then 0x5D
  if (!cap1.begin(0x5A)) {                                          
    Serial.println("MPR121 not found, check wiring?");             
    while (1);
  }                                

  Serial.println("MPR121 found!");  


    if (!cap2.begin(0x5C)) {                                          
  Serial.println("MPR121 not found, check wiring?");             
   while (1);
  }                                

    Serial.println("MPR121 found!");

  
  // I2C init
  //Serial.println("Starting up I2C");
  //Wire.begin();
  //Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
  //
  //Serial.println("starting I2C (Wire.begin)...");
  
#ifdef SERIAL_OSC
    SLIPSerial.begin(115200);   // set this as high as you can reliably run on your platform 
#endif
    
    // touch pads
    Serial.println("setting INPUT_PULLUP for capacitive touch pins... ");
    
    for(int i = 0; i < KEYLEN; i++) {
      pinMode(keys[i], INPUT_PULLUP);
    }   
} // setup ends //////////////////////////////////////////////////

// MAIN LOOP /////////////////////////////////////////////////////////

int colorCount = 0;

void loop() { 
      //Set the new color on the pixel.
    pixels.setPixelColor(0, Wheel(colorCount++));
    pixels.show();   
    //Cycle the colors at the end.
    if (colorCount > 255)
        colorCount = 0;
        
    sendCapacitiveA(); // from the 121
    sendCapacitiveB(); // from the 121
    sendCapacitiveOlimex(); // from the board

}

// FUNCTIONS /////////////////////////////////////////////


// Simple function to return a color in the rainbow
// Input a value 0 to 255 to get a color value.
uint32_t Wheel(byte WheelPos)
{
    //Assume the wheel value is less than 85, if so Green value is 0
    uint32_t returnColor = Adafruit_NeoPixel::Color((byte)(255 - (WheelPos * 3)), 0, (byte)(WheelPos * 3));

    //If we are greater than 170 Red value is 0
    if (WheelPos > 84 && WheelPos < 170)
    {
        WheelPos -= 85;
        returnColor = Adafruit_NeoPixel::Color(0, (byte)(WheelPos * 3), (byte)(255 - WheelPos * 3));
    }
    //Finally above 170 and Blue value is 0
    else if (WheelPos >= 170)
    {
        WheelPos -= 170;
        returnColor = Adafruit_NeoPixel::Color((byte)(WheelPos * 3), (byte)(255 - WheelPos * 3), 0);
    }

    return returnColor;
}

void connectWiFi(){
  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  // Print local IP address and start web server
  //  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// detect touch events and send over OSC //////////////

void sendCapacitiveA(void){ 
 // Get the currently touched pads
  currtouched0 = cap1.touched();
    
  for (uint8_t i=0; i<KEYLEN; i++) {
    // it if *is* touched and *wasnt* touched before, alert!
    if ((currtouched0 & _BV(i)) && !(lasttouched0 & _BV(i)) ) {
      Serial.print(i); Serial.println(" touched");

       ledState = !ledState;

              OSCMessage msg("/touch_121_A");
              msg.add(WiFi.macAddress().c_str()); // mac address of sender           
              msg.add((int32_t)i); // send which sensor was touched
  
              #ifdef WIFI_OSC
              udp.beginPacket(castIp, port);
            //bundle.send(udp);
              msg.send(udp);
              udp.endPacket();
             #endif

            String cap1 = "val for key num: " + String(i) + " === " + String(HIGH);
            Serial.println("OSC SENT "); Serial.println(cap0);       
    }
    // if it *was* touched and now *isnt*, alert!
    if (!(currtouched0 & _BV(i)) && (lasttouched0 & _BV(i)) ) {
      Serial.print(i); Serial.println(" released");

                  OSCMessage msg("/release_121_A");
              msg.add(WiFi.macAddress().c_str()); // mac address of sender           
              msg.add((int32_t)i); // send which sensor was touched
  
              #ifdef WIFI_OSC
              udp.beginPacket(castIp, port);
            //bundle.send(udp);
              msg.send(udp);
              udp.endPacket();
             #endif

            String cap1 = "val for key num: " + String(i) + " === " + String(LOW);
            Serial.println("OSC SENT "); Serial.println(cap1);       

    
    }
  }

  // reset our state
  lasttouched0 = currtouched0;


/*
  // comment out this line for detailed data from the sensor!
//  return;
  
  // debugging info, what
  Serial.print("\t\t\t\t\t\t\t\t\t\t\t\t\t 0x"); Serial.println(cap.touched(), HEX);
  Serial.print("Filt: ");
  for (uint8_t i=0; i<KEYLEN; i++) {
    Serial.print(cap.filteredData(i)); Serial.print("\t");
  }
  Serial.println();
  Serial.print("Base: ");
  for (uint8_t i=0; i<KEYLEN; i++) {
    Serial.print(cap.baselineData(i)); Serial.print("\t");
  }
  Serial.println();
*/  
  // put a delay so it isn't overwhelming
  delay(5);

  //return; 
  /////////////////////////////////////////////////////////////////////////////////////////

  //Serial.println(int(currtouched+pow(2,12)),BIN);

  //Serial.print("<");



    //Serial.println(">");

} //////////////////



void sendCapacitiveB(void){ 
 // Get the currently touched pads
  currtouched1    = cap2.touched();
    
  for (uint8_t i=0; i<KEYLEN; i++) {
    // it if *is* touched and *wasnt* touched before, alert!
    if ((currtouched1 & _BV(i)) && !(lasttouched1 & _BV(i)) ) {
      Serial.print(i); Serial.println(" touched");

       ledState = !ledState;

              OSCMessage msg("/touch_121_B");
              msg.add(WiFi.macAddress().c_str()); // mac address of sender           
              msg.add((int32_t)i); // send which sensor was touched
  
              #ifdef WIFI_OSC
              udp.beginPacket(castIp, port);
            //bundle.send(udp);
              msg.send(udp);
              udp.endPacket();
             #endif

            String cap2 = "val for key num: " + String(i) + " === " + String(HIGH);
            Serial.println("OSC SENT "); Serial.println(cap2);       
    }
    // if it *was* touched and now *isnt*, alert!
    if (!(currtouched1 & _BV(i)) && (lasttouched1 & _BV(i)) ) {
      Serial.print(i); Serial.println(" released");

                  OSCMessage msg("/release_121_B");
              msg.add(WiFi.macAddress().c_str()); // mac address of sender           
              msg.add((int32_t)i); // send which sensor was touched
  
              #ifdef WIFI_OSC
              udp.beginPacket(castIp, port);
            //bundle.send(udp);
              msg.send(udp);
              udp.endPacket();
             #endif

            String cap2 = "val for key num: " + String(i) + " === " + String(LOW);
            Serial.println("OSC SENT "); Serial.println(cap2);       

    
    }
  }

  // reset our state
  lasttouched1 = currtouched1;


/*
  // comment out this line for detailed data from the sensor!
//  return;
  
  // debugging info, what
  Serial.print("\t\t\t\t\t\t\t\t\t\t\t\t\t 0x"); Serial.println(cap.touched(), HEX);
  Serial.print("Filt: ");
  for (uint8_t i=0; i<KEYLEN; i++) {
    Serial.print(cap.filteredData(i)); Serial.print("\t");
  }
  Serial.println();
  Serial.print("Base: ");
  for (uint8_t i=0; i<KEYLEN; i++) {
    Serial.print(cap.baselineData(i)); Serial.print("\t");
  }
  Serial.println();
*/  
  // put a delay so it isn't overwhelming
  delay(5);

  //return; 
  /////////////////////////////////////////////////////////////////////////////////////////

  //Serial.println(int(currtouched+pow(2,12)),BIN);

  //Serial.print("<");



    //Serial.println(">");

} //////////////////




void sendCapacitiveOlimex(void){
  
  for(int i = 0; i < KEYLEN; i++) {
    
    // read the state of the touch pad
    pressed[i] = touchRead(keys[i]);
    Serial.println(pressed[i]);
    
    
    //     if (pressed[i] < threshold) {
    if (touchRead(keys[i]) < threshold) {
      pressed[i] = HIGH;
    } else{
      pressed[i] = LOW;
    }
    
    // If the pin is touched:
    if (pressed[i] != lastTouchState[i]) {
      // reset the debouncing timer
      lastDebounceTime[i] = millis();
    }
    
    if ((millis() - lastDebounceTime[i]) > debounceDelay) {
      // whatever the reading is at, it's been there for longer than the debounce
      // delay, so take it as the actual current state:
      // if the touch state has changed:
      if (pressed[i] != touchState[i]) {
	touchState[i] = pressed[i];
	// Serial.println("-------------------------------------- ");
	digitalWrite(ledPin, HIGH); // toggle the led on any touch
	pixels.setBrightness(5);
	
	// only toggle the LED if the new touch state is high
	if (touchState[i] == HIGH) {
	  pixels.setBrightness(25);
	  ledState = !ledState;
	  
	  OSCMessage msg("/touch_esp32");
	  msg.add(WiFi.macAddress().c_str()); // mac address of sender
	  msg.add((int32_t)i); // send which sensor was touched
	  msg.add((int32_t)touchRead(keys[i])); // send which sensor was touched
	  //  pressed[i]
#ifdef WIFI_OSC
	  udp.beginPacket(castIp, port);
	  //bundle.send(udp);
	  msg.send(udp);
	  udp.endPacket();
#endif
	  
	  String cap = "val for key num: " + String(i) + " === " + String(pressed[i]);
	  Serial.println(""); Serial.println(cap);
	}
	
        if (touchState[7] == HIGH) {
	  digitalWrite(loopLedPin, loopLedState);
	  loopLedState = !loopLedState;
        }
        if (touchState[8] == HIGH) {
	  digitalWrite(shiftLedPin, shiftLedState);
	  shiftLedState = !shiftLedState;
        }
	
      }
    }
    
    // set the LED:
    digitalWrite(ledPin, ledState);
    
    // save the reading. Next time through the loop, it'll be the lastTouchState:
    lastTouchState[i] = pressed[i];
  
  }

} //////////////////

