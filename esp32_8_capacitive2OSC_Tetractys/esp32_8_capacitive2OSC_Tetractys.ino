
// esp32 capacitive touch interface /////////////////////////////////////////////////
// rob canning 2023 /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

const int ledPin = 23;
const int shiftLedPin = 25;
const int loopLedPin = 26;

const int threshold = 30;  // set the threshold
int ledState = LOW;         // the current state of the output pin
int shiftLedState = HIGH;         // the current state of the output pin
int loopLedState = LOW;         // the current state of the output pin

//#include <SPI.h> // Serial Peripheral Interface
#include <Wire.h> // allows you to communicate with I2C / TWI devices

// Wi-Fi SETTINGS /////////////
#include <WiFi.h>
const char* ssid     = "zavod rizoma_EXT";
const char* password = "cermozise";

// BROADCAST TO ALL IP / port
const IPAddress castIp = IPAddress(192,168,0,255);  // 255 FOR BROADCAST
const int port = 57120; // SUPERCOLLIDERS DEFAULT INCOMING OSC PORT
bool connected = false;

#include <WiFiUdp.h>
WiFiUDP udp;

// OSC and serial communications  ////////////////////////////

#define SERIAL_OSC
#define WIFI_OSC
#define BT_OSC

#include <OSCBundle.h>
#include <OSCMessage.h>
#include <OSCBoards.h>

/* OSC MSG channels */
OSCBundle bundle;

// SERIAL
  #ifdef BOARD_HAS_USB_SERIAL
#include <SLIPEncodedUSBSerial.h>
SLIPEncodedUSBSerial SLIPSerial( thisBoardsSerialUSB );
#else
#include <SLIPEncodedSerial.h>
SLIPEncodedSerial SLIPSerial(Serial);
#endif

// Bluetooth ////////////////////////////////

#ifdef BT_OSC
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#include <SLIPEncodedSerial.h>
#include "BluetoothSerial.h"
#include "SLIPEncodedBluetoothSerial.h"

BluetoothSerial SerialBT;
SLIPEncodedBluetoothSerial SLIPBTSerial(SerialBT);
#endif

//////////////////////////////////////////
// CAPACITIVE TOUCH SETUP

byte keys[] = {32, 33, 27, 14, 12, 13, 4, 15};
byte KEYLEN = 8;
byte pressed[] = {0, 0, 0, 0, 0, 0, 0, 0};
byte lastTouchState[] = {0, 0, 0, 0, 0, 0, 0, 0};
byte touchState[] = {0, 0, 0, 0, 0, 0, 0, 0};
byte lastDebounceTime[] = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
int THRESH = 30; // capacitive noise threshold

///////////////////////////////////////////

void setup() {
  pinMode(ledPin, OUTPUT); //Define ledPin as output
  pinMode(shiftLedPin, OUTPUT); //Define ledPin as output  
  pinMode(loopLedPin, OUTPUT); //Define ledPin as output  
  digitalWrite(ledPin, ledState);
  digitalWrite(shiftLedPin, shiftLedState);
  digitalWrite(loopLedPin, loopLedState);
  Serial.begin(115200);
  WiFi.mode(WIFI_AP);
  connectWiFi();
  
  const String macAddress = WiFi.macAddress(); ;
  
  // I2C init
  Serial.println("Starting up I2C");
  Wire.begin();
  //Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
  //
  Serial.println("starting I2C (Wire.begin)...");
  
#ifdef SERIAL_OSC
    SLIPSerial.begin(115200);   // set this as high as you can reliably run on your platform 
#endif
    
#ifdef BT_OSC
    SerialBT.begin("grimore");
#endif
    
    // touch pads
    Serial.println("setting INPUT_PULLUP for capacitive touch pins... ");
    
    for(int i = 0; i < KEYLEN; i++) {
      pinMode(keys[i], INPUT_PULLUP);
    }   
} // setup ends //////////////////////////////////////////////////

// MAIN LOOP /////////////////////////////////////////////////////////

void loop() { 
  sendCapacitive();
}

// FUNCTIONS /////////////////////////////////////////////

void connectWiFi(){
  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  // Print local IP address and start web server
  //  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// detect touch events and send over OSC //////////////

void sendCapacitive(void){ 
  
  for(int i = 0; i < KEYLEN; i++) {  
    // read the state of the touch pad
    pressed[i] = touchRead(keys[i]);
    Serial.println(pressed[i]);
    
    if (pressed[i] < threshold) {
      pressed[i] = HIGH;
    } else{
      pressed[i] = LOW;
    }
    
    // If the pin is touched:
    if (pressed[i] != lastTouchState[i]) {
      // reset the debouncing timer
      lastDebounceTime[i] = millis();
    }
    
    if ((millis() - lastDebounceTime[i]) > debounceDelay) {
      // whatever the reading is at, it's been there for longer than the debounce
      // delay, so take it as the actual current state:
      
      // if the touch state has changed:
      if (pressed[i] != touchState[i]) {
	          touchState[i] = pressed[i];
	          // Serial.println("-------------------------------------- "); 
          	digitalWrite(ledPin, HIGH); // toggle the led on any touch
    
      // only toggle the LED if the new touch state is high
           if (touchState[i] == HIGH) {
	           ledState = !ledState;

	            OSCMessage msg("/touch");
            	msg.add(WiFi.macAddress().c_str()); // mac address of sender           
            	msg.add((int32_t)i); // send which sensor was touched
	
              #ifdef WIFI_OSC
	            udp.beginPacket(castIp, port);
          	//bundle.send(udp);
	            msg.send(udp);
	            udp.endPacket();
             #endif

            String cap = "val for key num: " + String(i) + " === " + String(pressed[i]);
            Serial.println(""); Serial.println(cap);   	  
          }

        if (touchState[7] == HIGH) {
	          digitalWrite(loopLedPin, loopLedState); 
            loopLedState = !loopLedState;
        } 
        if (touchState[8] == HIGH) {
	          digitalWrite(shiftLedPin, shiftLedState); 
            shiftLedState = !shiftLedState;
        } 

        }
    }
    
    // set the LED:
    digitalWrite(ledPin, ledState);    
    // save the reading. Next time through the loop, it'll be the lastTouchState:
    lastTouchState[i] = pressed[i];
  
  }

} //////////////////
