# TACTUS TOUCH SENSOR LAYOUT

// PLAY SENSORS // ----------------------------------

0   SAMPLE {orange}		[CTRL REVERSE] {magenta} [SHIFT: TRANSPOSE]	
1   SAMPLE 			[CTRL REVERSE] 		 [SHIFT: TRANSPOSE] 	
2   SAMPLE			[CTRL REVERSE]		 [SHIFT: TRANSPOSE] 	
3   SAMPLE  			[CTRL REVERSE]		 [SHIFT: TRANSPOSE] 	
4   SAMPLE  			[CTRL REVERSE]		 [SHIFT: TRANSPOSE]	
5   SAMPLE  			[CTRL REVERSE]		 [SHIFT: TRANSPOSE] 	
6   SAMPLE  			[CTRL REVERSE]		 [SHIFT: TRANSPOSE] 	
7   SAMPLE  			[CTRL REVERSE]		 [SHIFT: TRANSPOSE] 	 

//  F-KEY SENSORS // ----------------------------------

8   LOOP ON/OFF	{red/green}	[EMPTY]			 [SHIFT: +SAMPLE DIR]
9   SCRAMBLE SET 		[CTRL: REORDER SET]	 [SHIFT: +SCALE ] 	
10  CTRL  						 [SHIFT: TRANSPOSE]		
11  SHIFT  			[CTRL: TRANSPOSE]

// SCALES: // ------------------------------------

0: Major [red]
1: Minor [green]
2: melodic [blue]
3: minorPENTATONIC
4: IONIAN [PYTHAGOREAN]
5: WHOLETONE
6: CHROMATIC
7: GONG
8: INDIAN
9: EGYPTIAN

// TRANSPOSITION MODES: // -----------------------

RED:   		UNTRANSPOSED
GREEN: 		1 OCTAVE DOWN
BLUE:  		2 OCTAVES DOWN
YELLOW:		1 OCTAVES UP
CYAN:  		2 OCTAVES UP	
MAGENTA: 	RANDOM OCTAVE DISPLACEMENT



